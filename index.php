<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Sign-Up/Login Form</title>
  <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">


      <link rel="stylesheet" href="css/style.css">


</head>

<body>
  <div class="form">

      <ul class="tab-group">
        <li class="tab active"><a href="#signup">S'inscrire</a></li>
        <li class="tab"><a href="#login">S'identifier</a></li>
      </ul>

      <div class="tab-content">

                <!---S'inscrire----------------------------------------->

        <div id="signup">
          <h1>S'enregistrer</h1>

          <form action="inc/index.php" method="post">

          <div class="top-row">
            <div class="field-wrap">
              <label>
                Prénom<span class="req">*</span>
              </label>
              <input type="text" required autocomplete="off" id=firstname name=firstname  />
            </div>

            <div class="field-wrap">
              <label>
                Nom de famille<span class="req">*</span>
              </label>
              <input type="text"required autocomplete="off" id=lastname name=lastname/>
            </div>
          </div>

          <div class="field-wrap">
            <label>
              Adresse email<span class="req">*</span>
            </label>
            <input type="email"required autocomplete="off" id=mail name=mail value="<?php if(isset($mail)) {echo $mail;} ?>"/>
          </div>

          <div class="field-wrap">
            <label>
              Choissisez un mot de passe<span class="req">*</span>
            </label>
            <input type="password"required autocomplete="off" id=password name=password value="<?php if(isset($password)) {echo $password;} ?>"/>
          </div>

          <div class="field-wrap">
            <label>
              Confirmer le mot de passe<span class="req">*</span>
            </label>
            <input type="password"required autocomplete="off" id=password2 name=password2 value="<?php if(isset($password2)) {echo $password2;} ?>"/>
          </div>

          <button type="submit" class="button button-block" name="signup"/>Valider</button>

          </form>
          
          <?php
          if(isset($erreur))
          {
            echo $erreur;
          }
          ?>

        </div>   <!---end------------->

                <!---Se connecter ------------------------------------>

        <div id="login">
          <h1>Se connecter</h1>

          <form action="/" method="post">

            <div class="field-wrap">
            <label>
              Adresse mail<span class="req">*</span>
            </label>
            <input type="email"required autocomplete="off" id=mailconnect name=mailconnect/>
          </div>

          <div class="field-wrap">
            <label>
              Mot de passe<span class="req">*</span>
            </label>
            <input type="password"required autocomplete="off" id=passwordconnect name=passwordconnect/>
          </div>

          <p class="forgot"><a href="#">Oublié le mot de passe ?</a></p>

          <button type="submit"  class="button button-block" name="login"/>Valider</button>

          </form>

        </div> <!---end---------->

      </div><!-- tab-content end-->

</div> <!-- /form -->
<?php
if(isset($erreur))
{
  echo $erreur;
}
?>

<!-- Java Script
================================================== -->
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script  src="js/index.js"></script>

</body>
</html>
